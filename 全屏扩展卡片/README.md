# 特效介绍 #
一个文字不停上下跳动的特效，类似Lodaing...
# 主要技术 #
 *box-reflect*: 镜面反射，需要注意的是这不是一个规范的属性，仅在Chrome等webkit内核中才能使用。在firefox浏览器中可以使用-moz-element()实现同样的效果

 *animation*: 制作文字上下移动的效果