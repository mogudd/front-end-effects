# 特效介绍 #
一个载有文章内容的卡片，点击后全屏显示文章
# 主要技术 #

# 开发过程疑点 #
1.当给book__content加上max-width后header-image的width也改变了

**实际上是用了div的width为auto时特性实现header-image自动改变宽度。当book_content用用max-width属性(750px)后，子元素的最大宽度就变为了750px。于是父元素的宽度就变为了750px，header-image的width:auto就会自动铺满容器宽度**